package com.app.henry.goldenbox.interfaces
import android.arch.lifecycle.LiveData
import com.app.henry.goldenbox.model.entity.Money

interface AsyncResult {
    fun onComplete(dataset: LiveData<List<Money>>)
}
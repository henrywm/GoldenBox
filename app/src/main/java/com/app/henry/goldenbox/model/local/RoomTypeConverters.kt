package com.app.henry.goldenbox.model.local

import android.arch.persistence.room.TypeConverter
import java.util.Date

class RoomTypeConverters{

    @TypeConverter
    fun longToDate(date: Long?): Date? {
        return if (date == null) null else Date(date)
    }

    @TypeConverter
    fun dateToLong(date: Date?): Long? {
        return date?.time
    }

}
package com.app.henry.goldenbox.view.add_money

import android.app.DatePickerDialog
import android.arch.lifecycle.ViewModelProviders
import android.content.Intent
import android.os.Bundle
import android.support.design.widget.Snackbar
import android.util.Log
import android.view.View
import android.widget.Toast
import com.app.henry.goldenbox.BaseActivity
import com.app.henry.goldenbox.R
import com.app.henry.goldenbox.model.entity.Money
import com.app.henry.goldenbox.view.moneys.HomeActivity
import com.app.henry.goldenbox.view.moneys.MoneyViewModel
import kotlinx.android.synthetic.main.activity_add_money.*
import java.text.SimpleDateFormat
import java.util.*

class AddMoneyActivity: BaseActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_add_money)

    }
    fun save(v: View) {
        val title = title_ed.text.toString()
        val amount = if(amount_ed.text.toString().trim() != "") amount_ed.text.toString().toFloat() else 0f
        val sdf = SimpleDateFormat("dd.MM.yyyy")
        val date = sdf.parse(date_ed.text.toString())
        val type = if(money_type_select.selectedItem.toString() == "Income") Money.INCOME else Money.EXPENSE
        val paid = if(paid_check.isChecked) Money.PAID else Money.NOT_PAID
        val money = Money(title, amount, date, type, paid)
        if(emptyFields(money)){
            val moneyViewModel = ViewModelProviders.of(this).get(MoneyViewModel::class.java)
            moneyViewModel.save(money)
            changeActivity()
        }else{
            Snackbar.make(v,"Required fields are empty!", Snackbar.LENGTH_LONG).show()
        }
    }
    private fun changeActivity() {
        Toast.makeText(this,"Money saved successfully!", Toast.LENGTH_LONG).show()
        startActivity(Intent(applicationContext, HomeActivity::class.java))
        this.finish()
    }
    fun openDatePickerDialog(v: View){
        val cal = Calendar.getInstance()
        val datePicker = DatePickerDialog.OnDateSetListener{ _, year, month, day ->
            cal.set(year,month,day)
            val dateFormat = "dd.MM.yyyy"
            val sdf = SimpleDateFormat(dateFormat, Locale.US)
            date_ed.setText(sdf.format(cal.time))
        }
        DatePickerDialog(this, datePicker,
                cal.get(Calendar.YEAR),
                cal.get(Calendar.MONTH),
                cal.get(Calendar.DAY_OF_MONTH)).show()
    }


    private fun emptyFields(money: Money): Boolean{
        money.let {
            if(it.title?.trim() != "" &&
               it.amount.toString().trim() != "" &&
               it.date?.toString()?.trim() != "") {
                return true
            }
        }
        return false
    }

}
package com.app.henry.goldenbox.view.moneys

import android.support.v7.widget.RecyclerView
import android.view.View
import com.app.henry.goldenbox.R
import com.app.henry.goldenbox.model.entity.Money
import com.app.henry.goldenbox.interfaces.ItemClickListener
import com.app.henry.goldenbox.interfaces.ItemLongClickListener
import kotlinx.android.synthetic.main.money_item.view.*

class MoneyListViewHolder(val view: View, private val clickListener: ItemClickListener, private val onLongClickListener: ItemLongClickListener):
        RecyclerView.ViewHolder(view), View.OnClickListener, View.OnLongClickListener{
    init {
        view.setOnClickListener(this)
        view.setOnLongClickListener(this)
    }

    fun bind(money: Money){
        view.title.text = money.title
        view.date.text = money.date.toString()
        view.amount.text = "R$${money.amount.toString()}"
        if(money.type == Money.INCOME){
            view.amount.setTextColor(view.resources.getColor(R.color.green))
        }else{
            view.amount.setTextColor(view.resources.getColor(R.color.red))
        }
        /*if(money.paid == Money.PAID){
            view.isPaid.text = "Paid"
            view.isPaid.setTextColor(view.resources.getColor(R.color.colorAccent))
        }else{
            view.isPaid.text = "Not paid"
            view.isPaid.setTextColor(view.resources.getColor(R.color.colorAccent))
        }*/
    }

    override fun onClick(v: View?) {
        clickListener.onItemClick(view, adapterPosition)
    }
    override fun onLongClick(v: View?): Boolean {
        onLongClickListener.onItemLongClick(view, adapterPosition)
        return false
    }
}

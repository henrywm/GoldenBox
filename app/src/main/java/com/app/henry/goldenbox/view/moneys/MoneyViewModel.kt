package com.app.henry.goldenbox.view.moneys
import android.app.Application
import android.arch.lifecycle.AndroidViewModel
import android.arch.lifecycle.LiveData
import com.app.henry.goldenbox.model.entity.Money
import com.app.henry.goldenbox.repository.MoneyRepository

class MoneyViewModel(val app: Application): AndroidViewModel(app){

    private var moneyRepository: MoneyRepository? = null
    private var moneyList: LiveData<List<Money>>? = null

    init {
        moneyRepository = MoneyRepository(app)
        moneyList = moneyRepository!!.loadAll()
    }

    fun save(money: Money) = moneyRepository?.save(money)
    fun moneys(): LiveData<List<Money>> = moneyList!!
    fun delete(money: Money) = moneyRepository?.delete(money)
}